import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@ComponentScan("com.gvs.apps.plain")
public class App
{
    public static void main(String[] args) throws InterruptedException {

        App app = new App();
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(app.getClass());
        DataSource ds = applicationContext.getBean(DataSource.class);
        try {
            ds.getConnection().getMetaData().getDatabaseProductName();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        while (true) {
            System.out.println("Waiting");
            synchronized(app) {
                app.wait(5000);
            }
        }
    }

}
