package com.gvs.apps.plain.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.util.StringUtils;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class DataSources {

    private String vcap_services;

    DataSources(Environment environment) {
        vcap_services = environment.getProperty("VCAP_SERVICES");
    }

    @Bean
    public DataSource getDataSource() throws IOException {
        System.out.println(String.format("VCAP_SERVICES: %s", vcap_services));
        if (vcap_services == null || StringUtils.isEmptyOrWhitespaceOnly(vcap_services)) {
            return new HikariDataSource();
        } else {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(vcap_services);
            String jdbcURL = root.at("/p.mysql/jdbcUrl").asText();
            System.out.println(String.format("JDBC URL: %S", jdbcURL));
            HikariDataSource ds = new HikariDataSource();
            ds.setJdbcUrl(jdbcURL);

            return ds;
        }
    }
}